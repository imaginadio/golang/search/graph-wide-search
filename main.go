package main

import "fmt"

type Graph struct {
	Name     string
	IsStart  bool
	IsFinish bool
	Checked  bool
	Deep     int
}

type Node struct {
	Value *Graph
	Next  *Node
}

type Queue struct {
	first  *Node
	last   *Node
	length int
}

func (q *Queue) Push(g *Graph) {
	node := Node{
		Value: g,
	}

	if q.last != nil {
		q.last.Next = &node
	}
	q.last = &node
	q.length++

	if q.first == nil {
		q.first = q.last
	}
}

func (q *Queue) Pop() *Graph {
	var result *Graph
	if q.first != nil {
		result = q.first.Value
		q.first = q.first.Next
		q.length--
	}
	return result
}

func main() {
	you := Graph{
		Name:    "you",
		IsStart: true,
	}

	alice := Graph{
		Name: "Alice",
	}

	claire := Graph{
		Name: "Claire",
	}

	bob := Graph{
		Name: "Bob",
	}

	peggy := Graph{
		Name: "Peggy",
	}

	jhonny := Graph{
		Name: "Jhonny",
	}

	anuj := Graph{
		Name:     "Anuj",
		IsFinish: true,
	}

	tom := Graph{
		Name: "Tom",
	}

	m := make(map[string][]*Graph)

	m[you.Name] = append(m[you.Name], &claire)
	m[you.Name] = append(m[you.Name], &bob)
	m[you.Name] = append(m[you.Name], &alice)
	m[alice.Name] = append(m[alice.Name], &peggy)
	m[bob.Name] = append(m[bob.Name], &peggy)
	m[bob.Name] = append(m[bob.Name], &anuj)
	m[claire.Name] = append(m[claire.Name], &jhonny)
	m[claire.Name] = append(m[claire.Name], &tom)

	q := Queue{}
	q.Push(&you)

	for q.length > 0 {
		v := q.Pop()
		fmt.Println("Checking", v.Name, ", deep is", v.Deep)
		if v.IsFinish {
			fmt.Printf("Found last element: %s, deep is: %d", v.Name, v.Deep)
			return
		}

		for _, el := range m[v.Name] {
			if !el.Checked {
				el.Deep = v.Deep + 1
				q.Push(el)
			}
		}
	}

	fmt.Println("Graph not found")
}
